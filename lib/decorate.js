/**
 *
 * decorate
 *
 **/

var __decorate = (this && this.__decorate) || function __decorate(decorators, target, key, desc) {
  switch (arguments.length) {
    case 2: /* class / constructor parameter */
      return decorators.reduceRight(function _decoratorExecutor(lastValue, decorator) {
        return (decorator && decorator(lastValue)) || lastValue;
      }, target);
    case 3: /* property */
      var desc = Object.getOwnPropertyDescriptor(target, key) || {};
      var pkey = '_property_' + key;
      var descriptor = {
        enumerable: true,
        get: desc.get || (desc.set && function _unknownPropertyGetter() {}) || function _propertyGetter() { return this[pkey]; },
        set: desc.set || (desc.get && function _unknownPropertySetter(value) {}) || function _propertySetter(value) {
          if (!this[pkey]) {
            Object.defineProperty(this, pkey, { /* enumerable: false */ writable: true, value: value });
          } else {
            this[pkey] = value;
          }
        }
      };

      descriptor = decorators.reduceRight(function _decoratorExecutor(lastDescriptor, decorator) {
        return (decorator && decorator(target, key, lastDescriptor)) || lastDescriptor;
      }, descriptor);

      if (descriptor.writable !== void 0) {
        if (!descriptor.writable) {
          descriptor.set = function _emptyPropertySetter(value) {};
        }
        delete descriptor.writable;
      }

      Object.defineProperty(target, key, descriptor);
      break;
    case 4: /* method / method parameter */
      return decorators.reduceRight(function _decoratorExecutor(lastDescriptor, decorator) {
        return (decorator && decorator(target, key, lastDescriptor)) || lastDescriptor;
      }, desc);
  }
};

var __param = (this && this.__param) || function __param(paramIndex, decorator) {
  return function _paramDecorator(target, key, descriptor) {
    return decorator(target, key, descriptor, paramIndex);
  }
};

var manualDecorate = function __decorate() {
  console.warn('WIP');
};

var Reflect = {};
Reflect.decorate = function () {
  var args = Array.prototype.slice.call(arguments.callee.caller.arguments);

  return __decorate.apply(void 0, args);
}

exports.__decorate = __decorate;
exports.__param = __param;

exports.decorate = manualDecorate;
exports.Reflect = Reflect;
